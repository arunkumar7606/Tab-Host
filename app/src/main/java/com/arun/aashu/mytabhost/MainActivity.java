package com.arun.aashu.mytabhost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    TabHost tb;
    TabHost.TabSpec tabSpec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tb=findViewById(R.id.tabhost);
        tb.setup();

        //store the refrence of first tab
        //First tab
       tabSpec=tb.newTabSpec("First Tab");
        tabSpec.setIndicator("First");
        tabSpec.setContent(R.id.tab1);
        tb.addTab(tabSpec);


        //Second tab
        tabSpec=tb.newTabSpec("Second Tab");
        tabSpec.setIndicator("Two");
        tabSpec.setContent(R.id.tab2);
        tb.addTab(tabSpec);


        tabSpec=tb.newTabSpec("Third Tab");
        tabSpec.setIndicator("Third");
        tabSpec.setContent(R.id.tab3);
        tb.addTab(tabSpec);


        tabSpec=tb.newTabSpec("Fourth Tab");
        tabSpec.setIndicator("Fourth");
        tabSpec.setContent(R.id.tab4);
        tb.addTab(tabSpec);




    }
}
